package main

import (
	"bytes"
	"html/template"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

const html = `
<!DOCTYPE html>
<html>
	<head>
		<title>Hello World</title>
	</head>
	<body>
		<h1>Hello from AWS Lambda</h1>
	</body>
</html>
`

var (
	// Buffer contains the io.Writer interface needed by template.Execute
	buf bytes.Buffer

	// Must will handle errors. This allows us to declare a new template
	// outside of the Lambda Handler. Important for speed and caching.
	t = template.Must(template.New("HTML Page").Parse(html))

	// Default return Headers
	headers = map[string]string{"Content-Type": "text/html"}
)

func init() {
	t.Execute(&buf, nil)
}

// https://docs.aws.amazon.com/lambda/latest/dg/best-practices.html
func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	return events.APIGatewayProxyResponse{
		Headers:    headers,
		Body:       buf.String(),
		StatusCode: 200,
	}, nil
}

func main() {
	lambda.Start(handler)
}
